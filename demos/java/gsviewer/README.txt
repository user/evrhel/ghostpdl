This project is a viewer is designed to load PDF documents, but
can also load other files like PostScript files and distill them
into a PDF document to be opened.

When the document is first loaded, every page is rendered at
a low resolution. These will be displayed as thumbnails on the
left side. All loadingg is done in the com.artifex.gsjava.Document
class. As the user changes the position of the viewport, the
viewer will dynamically load high resolution images based on
what page the user is viewing. This is done in the
com.artifex.gsviewer.ViewerController.SmartLoader class. This
also handles rendering zoomed images if necessary. Only pages
which need to be loaded at a high resolution are loaded, and
zoomed pages are unloaded when no longer necessary to save memory.

Required libraries in this directory for the library to work:

-= WINDOWS =-

gpdldll64.dll
gs_gni.dll


-= LINUX =-

libgpdl.so (this would have been built as a link to another file, so
it shoud be copied into this directory and renamed to libgpdl.so)
gs_jni.so

On Linux, when using OpenJDK, the property "assistive_technologies" may
need to be modified for the Java code to build. It can be modified by
editing the "accessibility.properties" file. This is located at:

/etc/java-8-openjdk/accessibility.properties

Additionally, to start the application, start.sh should be executed as it
sets up the shared library paths.


Building:

-= WINDOWS =-

Ensure both gs_jni.dll, gpdldll64.dll, and gsjava.jar have been built. Then,
run the build.bat script. To run, open gsviewer.jar either through File
Explorer or in the command line through the following command:

java -jar gsviewer.jar

-= LINUX =-

Ensure Ghoscript has been built.
Run the build.sh script. This automatically will build the
gs_jni.co, gsjava.jar, and copy the files to the needed
directories. gsviewer.jar will be outputted in this directory.